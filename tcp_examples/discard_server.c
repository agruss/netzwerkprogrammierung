/*-
 * Copyright (c) 2013 Michael Tuexen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "Socket.h"

#define BUFFER_SIZE (1<<16)
#define MAX_CLIENTS 50
#define PORT "9"

int
main(int argc, char **argv)
{
	int fd, cfd, i, j, max_fd;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	ssize_t len;
	fd_set rset, aset;
	char buf[BUFFER_SIZE];
	int clients[MAX_CLIENTS];
	struct addrinfo hints, *res;
	
	memset(&clients, -1, MAX_CLIENTS * sizeof(int));

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	
	Getaddrinfo(argv[1], PORT, &hints, &res);
	
	fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	Bind(fd, res->ai_addr, res->ai_addrlen);
	Freeaddrinfo(res);
	
	Listen(fd, 1);
	
	FD_ZERO(&aset);
	FD_SET(fd, &aset);
	max_fd = fd;
	while(1) {
		rset = aset;
		printf("Server in listen state\n");
		Select(max_fd + 1, &rset, (fd_set *) NULL, (fd_set *) NULL,(struct timeval *) NULL);
		if(FD_ISSET(fd, &rset)) {
			memset((void *) &client_addr, 0, sizeof(client_addr));
			client_addr_len = (socklen_t) sizeof(client_addr);
			cfd = Accept(fd, (struct sockaddr *) &client_addr, &client_addr_len);
						
			for(i = 0; i < MAX_CLIENTS; i++) {
				if(clients[i] == -1) {
					clients[i] = cfd;
					
					if(clients[i] > max_fd)
						max_fd = clients[i];
					
					printf("Connected to server\n");
					FD_SET(clients[i], &aset);
					break;
				}
			}
			if(i == MAX_CLIENTS)
				Close(cfd);
		}
		for(i = 0; i < MAX_CLIENTS; i++) {
			if(clients[i] != -1 && FD_ISSET(clients[i], &rset)) {
				len = Recv(clients[i], (void *) buf, sizeof(buf), 0);
				
				if(len > 0)
					printf("Discarded bytes: %i\n", len);
				else {
					Close(clients[i]);
					clients[i] = -1;
					
					max_fd = fd;
					for(j = 0; j < MAX_CLIENTS; j++)
						if(clients[j] > max_fd)
							max_fd = clients[j];
					
					printf("Connection closed\n");
				}
			}
		}
		memset(buf, 0, sizeof(buf));		
	}

	Close(fd);
	
	return(0);
}
