/*-
 * Copyright (c) 2013 Michael Tuexen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "Socket.h"

#define BUFFER_SIZE  (1<<16)
#define MESSAGE_SIZE (9216)

int
main(int argc, char **argv)
{
	int fd = -1, done = 0, n;
	fd_set rset;
	char buf[BUFFER_SIZE];
	struct addrinfo hints, *res, *res0;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	
	Getaddrinfo(argv[1], argv[2], &hints, &res0);
	
	for(res = res0; res != NULL; res = res->ai_next) {
		fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if(connect(fd, res->ai_addr, res->ai_addrlen) >= 0)
			break;
		Close(fd);
		fd = -1;
	}
	if(fd < 0) {
		puts("connection failed");
		exit(-1);
	}

	while(!done) {
		FD_ZERO(&rset);
		FD_SET(0, &rset);
		FD_SET(fd, &rset);
		Select(fd + 1, &rset, (fd_set *) NULL, (fd_set *) NULL,(struct timeval *) NULL);
	  
		if(FD_ISSET(0, &rset)) {
			n = Read(0, (void *) buf, sizeof(buf));
			if (n == 0) {
				Shutdown(fd, SHUT_WR);
			}
			else {
				printf("stdin: %s\n", buf);
				Send(fd, (const void *) buf, (size_t) n, 0);
			}
		}
		
		if(FD_ISSET(fd, &rset)) {
			n = Recv(fd, (void *) buf, sizeof(buf), 0);
			if(n == 0) {
				done = 1;
			}
			else {
				printf("received from server: %s\n", buf);
			}
		}
		memset(buf, 0, sizeof(buf));
	}
		
	Close(fd);
	Freeaddrinfo(res);
	return(0);
}
