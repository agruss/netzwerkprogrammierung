/*-
 * Copyright (c) 2013 Michael Tuexen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "Socket.h"

#define BUFFER_SIZE (1<<16)
#define PORT "19"

int generate_chars(int cfd, char* buf, int size) {
	int i, j;
	for(i = 0, j = 65; i < 27; i++, j++)
		buf[i] = j;
	buf[26] = '\n';
	if(send(cfd, (const void *) buf, size, 0) < 0)
		return -1;
	
	return 0;
}

static void* handle_connection(void *arg) {
	int cfd = *((int *) arg);
	int len, done = 0;
	char buf[BUFFER_SIZE];
	
	fd_set rset, wset;
	
	Pthread_detach(pthread_self());
	printf("Thread detached\n");

	FD_ZERO(&wset);
	FD_ZERO(&rset);

	while(!done) {
		FD_SET(cfd, &wset);
		FD_SET(cfd, &rset);

		Select(cfd + 1, &rset, &wset, (fd_set *) NULL,(struct timeval *) NULL);				
		if(FD_ISSET(cfd, &wset)) {
			if(generate_chars(cfd, buf, 27) < 0)
				done = 1;
		}
		if(FD_ISSET(cfd, &rset)) {
			len = Recv(cfd, (void *) buf, sizeof(buf), 0);
			if(len == 0) {
				done = 1;
			}
		}
	}
	
	Close(cfd);
	printf("Thread: Connection closed\n");
	
	return NULL;
}

int
main(int argc, char **argv)
{
	int fd;
	int *cfd;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	pthread_t client;
	struct addrinfo hints, *res;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	
	Getaddrinfo(argv[1], PORT, &hints, &res);
	
	fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	Bind(fd, res->ai_addr, res->ai_addrlen);
	Freeaddrinfo(res);

	Listen(fd, 1);
	
	while(1) {
		printf("Server in listen state\n");
		memset((void *) &client_addr, 0, sizeof(client_addr));
		client_addr_len = (socklen_t) sizeof(client_addr);
		cfd = (int *) malloc(sizeof(int));
		*cfd = Accept(fd, (struct sockaddr *) &client_addr, &client_addr_len);

		printf("Connected to server\n");
		if(Pthread_create(&client, NULL, &handle_connection, (void *) cfd) != 0) {
			printf("pthread_create error!");
		}		
	}
	Close(fd);
	
	return(0);
}
