/*-
 * Copyright (c) 2013 Michael Tuexen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "Socket.h"

#define BUFFER_SIZE (1<<16)
#define PORT "13"

int
main(int argc, char **argv)
{
	int fd, cfd;
	time_t ltime;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	struct addrinfo hints, *res;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	
	Getaddrinfo(argv[1], PORT, &hints, &res);
	
	fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	Bind(fd, res->ai_addr, res->ai_addrlen);
	Freeaddrinfo(res);

	Listen(fd, 1);
	
	while(1) {
	  printf("Server in listen state\n");
		memset((void *) &client_addr, 0, sizeof(client_addr));
		client_addr_len = (socklen_t) sizeof(client_addr);
		cfd = Accept(fd, (struct sockaddr *) &client_addr, &client_addr_len);
		printf("Connected to server\n");
			
		time(&ltime);
		Send(cfd, (const void *) ctime(&ltime), 26, 0);
		printf("Time sent\n");
		
		Close(cfd);
		printf("Connection closed\n");
	}
	Close(fd);
	
	return(0);
}
