#include "Socket.h"

int Socket(int family, int type, int protocol) {
  int fd;
  
  if ((fd  = socket(family, type, protocol)) < 0) {
    perror("socket");
    exit(-1);
  }
  
  return fd;
}

ssize_t Sendto(int fd, const void *buf, size_t buflen, int flags, const struct sockaddr *to, socklen_t addrlen) {
  ssize_t bytes;
  
  if ((bytes = sendto(fd, buf, buflen, flags, to, addrlen)) < 0) {
    perror("sendto");
    exit(-1);
  }
  
  return bytes;
}

ssize_t Recvfrom(int fd, void *buf, size_t buflen, int flags, struct sockaddr *from, socklen_t *addrlen) {
  ssize_t len;
  
  if ((len = recvfrom(fd, buf, buflen, flags, from, addrlen)) < 0) {
    perror("recvfrom");
    exit(-1);
  }
  
  return len;
}

int Close(int fd) {
  if(close(fd) < 0) {
    perror("close");
    exit(-1);
  }
  
  return 0;
}

int Bind(int fd, const struct sockaddr *addr, socklen_t addrlen) {
  if (bind(fd, addr, addrlen) != 0) {
    perror("bind");
    exit(-1);
  }
  
  return 0;
}

