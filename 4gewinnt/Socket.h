#ifndef SOCKET_H_
#define SOCKET_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <netdb.h>

int Socket(int family, int type, int protocol);
ssize_t Sendto(int fd, const void *buf, size_t buflen, int flags, const struct sockaddr *to, socklen_t addrlen);
ssize_t Recvfrom(int fd, void *buf, size_t buflen, int flags, struct sockaddr *from, socklen_t *addrlen);
int Close(int fd);
int Bind(int fd, const struct sockaddr *addr, socklen_t addrlen);

int Listen(int fd, int backlog);
int Accept(int fd, struct sockaddr* addr, socklen_t *len);
int Connect(int fd, const struct sockaddr *addr, socklen_t len);
ssize_t Send(int fd, const void *buf, size_t buflen, int flags);
ssize_t Recv(int fd, void *buf, size_t buflen, int flags);
int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);	
ssize_t Read(int fd, void *buf, size_t nbyte);
int Shutdown(int socket, int how);
int Pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
int Pthread_detach(pthread_t thread);

int Getaddrinfo(const char *hostname, const char *servname, const struct addrinfo *hints, struct addrinfo **res);
void Freeaddrinfo(struct addrinfo *ai);
int Getnameinfo(const struct sockaddr *sa, socklen_t salen, char *host, size_t hostlen, char *serv, size_t servlen, int flags);

#endif
