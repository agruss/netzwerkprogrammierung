#include "Socket.h"

int Socket(int family, int type, int protocol) {
  int fd;
  
  if ((fd  = socket(family, type, protocol)) < 0) {
    puts("socket error");
    exit(-1);
  }
  
  return fd;
}

ssize_t Sendto(int fd, const void *buf, size_t buflen, int flags, const struct sockaddr *to, socklen_t addrlen) {
  ssize_t bytes;
  
  if ((bytes = sendto(fd, buf, buflen, flags, to, addrlen)) < 0) {
    puts("sendto error");
    exit(-1);
  }
  
  return bytes;
}

ssize_t Recvfrom(int fd, void *buf, size_t buflen, int flags, struct sockaddr *from, socklen_t *addrlen) {
  ssize_t len;
  
  if ((len = recvfrom(fd, buf, buflen, flags, from, addrlen)) < 0) {
    puts("recvfrom error");
    exit(-1);
  }
  
  return len;
}

int Close(int fd) {
  if(close(fd) < 0) {
    puts("close error");
    exit(-1);
  }
  
  return 0;
}

int Bind(int fd, const struct sockaddr *addr, socklen_t addrlen) {
  if (bind(fd, addr, addrlen) != 0) {
    //puts("bind error");
    perror("bind error");
    exit(-1);
  }
  
  return 0;
}

int Listen(int fd, int backlog) {
	if(listen(fd, backlog) < 0) {
		puts("listen error");
		exit(-1);
	}	
	
	return 0;
}

int Accept(int fd, struct sockaddr* addr, socklen_t *len) {
	int filedes;
	
	if((filedes = accept(fd, addr, len)) < 0) {
		puts("accept error");
		exit(-1);
	}
	
	return filedes;
}

int Connect(int fd, const struct sockaddr *addr, socklen_t len) {
	if(connect(fd, addr, len) < 0) {
		//puts("connect error");
		perror("connect error");
		exit(-1);
	}

	return 0;
}

ssize_t Send(int fd, const void *buf, size_t buflen, int flags) {
	ssize_t bytes;

	if((bytes = send(fd, buf, buflen, flags)) < 0) {
		puts("send error");
		exit(-1);
	}

	return bytes;
}

ssize_t Recv(int fd, void *buf, size_t buflen, int flags) {
	ssize_t bytes;

	if((bytes = recv(fd, buf, buflen, flags)) < 0) {
		puts("recv error");
		exit(-1);
	}

	return bytes;
}

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout) {
	int fd;
	
 	if((fd = select(nfds, readfds, writefds, exceptfds, timeout)) < 0) {
      		perror("select error");
      		exit(-1);
  	}

  	return fd;
}

ssize_t Read(int fd, void *buf, size_t nbyte) {
  	ssize_t bytes;

	if((bytes = read(fd, buf, nbyte)) < 0) {
	   	puts("read error");
		exit(-1);
	}
	
	return bytes;
}

int Shutdown(int socket, int how) {
	if(shutdown(socket, how) < 0) {
		puts("shutdown error");
		exit(-1);
	}
	
	return 0;
}

int Pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) {
	if(pthread_create(thread, attr, start_routine, arg) < 0) {
		puts("pthread_create error");
		exit(-1);
	}
	
	return 0;
}

int Pthread_detach(pthread_t thread) {
	if(pthread_detach(thread) < 0) {
		puts("pthread_detatch error");
		exit(-1);
	}
	
	return 0;
}

int Getaddrinfo(const char *hostname, const char *servname, const struct addrinfo *hints, struct addrinfo **res) {
	int error = getaddrinfo(hostname, servname, hints, res);
	if(error) {
		if (error == EAI_SYSTEM)
			fprintf(stderr, "%s\n", strerror(errno));
		else
			fprintf(stderr, "%s\n", gai_strerror(error));
		return -1;
	}
	
	return 0;
}

void Freeaddrinfo(struct addrinfo *ai) {
	freeaddrinfo(ai);
}

int Getnameinfo(const struct sockaddr *sa, socklen_t salen, char *host, size_t hostlen, char *serv, size_t servlen, int flags) {
	if(getnameinfo(sa, salen, host, hostlen, serv, servlen, flags) < 0) {
		puts("getnameinfo error");
		exit(-1);
	}
	
	return 0;
}
