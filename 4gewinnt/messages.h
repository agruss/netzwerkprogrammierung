#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <inttypes.h>
#include "Socket.h"

#define BUFFER 1<<16

#define HELLO_TYPE 0x0000
#define HELLOACK_TYPE 0x0001
#define PEERINFO_TYPE 0x0002
#define PEERINFOACK_TYPE 0x0003

#define TURN_TYPE 0x1000
#define TURNACK_TYPE 0x1010

#define ERROR_TYPE 0x0200
#define HEARTBEAT_TYPE 0x0201
#define HEARTBEATACK_TYPE 0x0202

struct message {
	uint16_t type;
	uint16_t length;
	uint8_t value[];
};

#pragma pack(1)
struct hello {
	uint16_t type;
	uint16_t length;
  
	struct in_addr ip_address;
  
	uint16_t port;
	uint16_t reserved;

	char name[];
};
#pragma pack(0)

struct helloACK {
	uint16_t type;
	uint16_t length;
};

struct peerinfo {
	uint16_t type;
	uint16_t length;
	
	struct in_addr ip_address;
	
	uint16_t port;
	uint16_t start;
	
	char name[];
};

struct peerinfoACK {
	uint16_t type;
	uint16_t length;
};


struct heartbeat {
	uint16_t type;
	uint16_t length;
};

struct heartbeatACK {
	uint16_t type;
	uint16_t length;
};

struct error {
	uint16_t type;
	uint16_t length;
};

int handleRecv(int fd, struct message *msg, int len_recv);

int checkRecv(int fd, struct message *msg, int len_recv);
int checkType(int fd, struct message *msg);

int createPadding(int length);


void send_hello(int fd, struct in_addr own_addr, const char *name, const char* connect_addr, const char* port);
void send_helloACK(int fd);

void send_peerinfo(int fd, struct in_addr ip_address, uint16_t port, char *name, int start);
void send_peerinfoACK(int fd);

void send_heartbeat(void *fd);
void send_heartbeatACK(int fd);

void send_error(int fd);
#endif
