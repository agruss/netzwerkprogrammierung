#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "Socket.h"
#include "messages.h"
#include "lib/cblib.h"

#define MAX_CLIENTS 50


int registeredPeers = 0;
//struct client ;

struct client {
	int fd;
	struct in_addr ip_address;
	uint16_t port;
	
	int checkClient;
	
	struct timer *timer;
	
	char *name;
} clients[MAX_CLIENTS];

/**
 * Regelt den Verbindungsaufbau via TCP
 * 
 */
int createConnection(char *ip_address, char *port) {
	struct addrinfo hints, *res;
	int fd;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	
	Getaddrinfo(ip_address, port, &hints, &res);
	
	fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	Bind(fd, res->ai_addr, res->ai_addrlen);
	Freeaddrinfo(res);
	
	Listen(fd, 1);
	
	return fd;
}

/**
 * Resetted einen registrierten Player
 * 
 */
void resetClient(struct client *client) {
	Close(client->fd);
	
	client->fd = -1;
	client->port = -1;
	client->name = NULL;
	client->checkClient = -1;
	
	stop_timer(client->timer);
	delete_timer(client->timer);
	deregister_fd_callback(client->fd);
	
	registeredPeers--;
}

/**
 * Legt einen der Spieler als Startspieler fest
 * 
 */
void setPlayerStart(int *player1, int *player2) {
	*player1 = rand();
	*player2 = rand();
	
	if(*player1 > *player2) {
		*player1 = 1;
		*player2 = 0;
	}
	else {
		*player1 = 0;
		*player2 = 1;
	}
}

/**
 * Deregistriert den übergebenen Client
 * 
 */
void deregisterClient(struct client *client) {
	client->fd = -1;
	client->port = -1;
	client->name = NULL;
	client->checkClient = -1;
	
	stop_timer(client->timer);
	delete_timer(client->timer);
	deregister_fd_callback(client->fd);
	
	registeredPeers--;
}

/**
 * Regelt den Clientverkehr
 * 
 */
void handleClient(void *index) {
	int len_recv, i = *((int *) index);
	void *buffer;
	buffer = (void *) malloc(BUFFER);
printf("nun im handleClient\n");
	
	clients[i].checkClient--;
	if(clients[i].checkClient < 0) {
		deregisterClient(&clients[i]);
		return;
	}
	
	len_recv = Recv(clients[i].fd, (void *) buffer, (size_t) BUFFER, 0);
	if(handleRecv(clients[i].fd, (struct message *) buffer, len_recv) == -1) {
		puts("handleRecv error");
		return;
	}
}


int main(int argc, char **argv) {
	int fd, cfd, i, j, k, max_fd, len_recv;
	int startPlayer1, startPlayer2;
	
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	fd_set rset, aset;
	
	void *buffer = (void *) malloc(BUFFER);
	
	for(i = 0; i < MAX_CLIENTS; i++) {
		clients[i].fd = -1;
	}

	fd = createConnection(argv[1], argv[2]);
	init_cblib();
	
	FD_ZERO(&aset);
	FD_SET(fd, &aset);
	max_fd = fd;
	printf("Server started and is waiting for players\n");
	while(1) {
		rset = aset;

		Select(max_fd + 1, &rset, (fd_set *) NULL, (fd_set *) NULL,(struct timeval *) NULL);
		if(FD_ISSET(fd, &rset)) {
			memset((void *) &client_addr, 0, sizeof(client_addr));
			client_addr_len = (socklen_t) sizeof(client_addr);
			cfd = Accept(fd, (struct sockaddr *) &client_addr, &client_addr_len);
						
			for(i = 0; i < MAX_CLIENTS; i++) {
				if(clients[i].fd == -1) {
					len_recv = Recv(cfd, (void *) buffer, (size_t) BUFFER, 0);
					if(handleRecv(cfd, (struct message *) buffer, len_recv) == -1) {
						break;
					}
					
					// Player in Liste einfügen
					clients[i].fd = cfd;
					clients[i].port = ((struct hello *) buffer)->port;
					clients[i].ip_address = ((struct hello *) buffer)->ip_address;
					clients[i].name = ((struct hello *)buffer)->name;
					
					clients[i].checkClient = 3;
					
					// Pruefen, ob Player FD größer ist als momentaner, maximaler FD
					if(clients[i].fd > max_fd)
						max_fd = clients[i].fd;
					
					printf("A player registered to the server\n");
					FD_SET(clients[i].fd, &aset);
					
					// Heartbeat Timer setzen & starten
					clients[i].timer = create_timer(send_heartbeat, (void *) &clients[i].fd, "Heartbeat Timer");

					start_timer(clients[i].timer, 5000);
					register_fd_callback(clients[i].fd, handleClient, (void *) &i);
					
					// Counter erhöhen
					registeredPeers++;
					break;
				}
			}
			if(i == MAX_CLIENTS)
				Close(cfd);
		}
		for(i = 0; i < MAX_CLIENTS; i++) {
			handle_events();
			if(clients[i].fd != -1) {
				for(j = i+1; j < MAX_CLIENTS; j++) {
					if(clients[j].fd != -1) {
						setPlayerStart(&startPlayer1, &startPlayer2);
						
						// Peerinfo senden
						send_peerinfo(clients[i].fd, clients[j].ip_address, clients[j].port, clients[j].name, startPlayer1);
						send_peerinfo(clients[j].fd, clients[i].ip_address, clients[i].port, clients[i].name, startPlayer2);

						// Warte auf PeerinfoACK
						len_recv = Recv(clients[i].fd, buffer, (size_t) BUFFER, 0);
						if(handleRecv(clients[i].fd, (struct message *) buffer, len_recv) == -1) {
							break;
						}
						
						len_recv = Recv(clients[j].fd, buffer, (size_t) BUFFER, 0);
						if(handleRecv(clients[j].fd, (struct message *) buffer, len_recv) == -1) {
							break;
						}
						// Beide Clients entfernen
						resetClient(&clients[i]);
						resetClient(&clients[j]);
						
						max_fd = fd;
						for(k = 0; k < MAX_CLIENTS; k++)
							if(clients[k].fd > max_fd)
								max_fd = clients[k].fd;
						
						printf("Connection closed\n");
					}
				}
					
			}
		}
	}

	Close(fd);
	
	return(0);
}
