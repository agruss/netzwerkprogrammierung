#include <stdio.h>
#include "lib/4clib.h"
#include "lib/cblib.h"
#include "messages.h"
#include "Socket.h"
	
#define BUFFER 1<<16
	
int fd;


char* itoa(int i, char b[]) {
    char const digit[] = "0123456789";
    char* p = b;
    if(i<0) {
        *p++ = '-';
        i = -1;
    }
    int shifter = i;
    do { //Move to where representation ends
        ++p;
        shifter = shifter/10;
    } while(shifter);
    *p = '\0';
    do { //Move back, inserting digits as u go
        *--p = digit[i%10];
        i = i/10;
    } while(i);
    return b;
}


void connect_peer(struct peerinfo *peer) {
	struct addrinfo hints, *res;
	char *own_addr = (char *) malloc(16);
	char *port = (char *) malloc(5);
	
	if(ntohs(peer->start) == 1) {
		// Testverbindung via TCP
		memset(&hints, 0, sizeof(hints));
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_family = PF_UNSPEC;
		hints.ai_protocol = IPPROTO_TCP;
		
		inet_ntop(AF_INET, (const void *) &peer->ip_address, own_addr, 16);
		port = itoa(ntohs(peer->port), port);
printf("start debug connect_peer\n");
printf("port:%s\n", port);
printf("ip_adress:%s\n", own_addr);
printf("end debug connect_peer\n");		
		Getaddrinfo(own_addr, port, &hints, &res);
		
		fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		Connect(fd, res->ai_addr, res->ai_addrlen);
		Freeaddrinfo(res);
	}
	printf("ab hier beginnt dann das spiel!!\n");
}

void connect_server(const char *own_addr, const char *name, const char *connect_addr, const char *port) {
	struct addrinfo hints, *res;
	struct in_addr ip_address;
	void* buffer;
	int len_recv;
	buffer = (void *) malloc(BUFFER);
	int helloack = -1, peerinfo = -1;

	// Verbindung zum Server
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	hints.ai_protocol = IPPROTO_TCP;
	
	Getaddrinfo(connect_addr, port, &hints, &res);
	
	fd = Socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	Connect(fd, res->ai_addr, res->ai_addrlen);
	Freeaddrinfo(res);

	// Hello Nachricht senden
	if(inet_pton(AF_INET, own_addr, &ip_address) < 1) {
		puts("inet_pton error");
		Close(fd);
		return;
	}
	
	send_hello(fd, ip_address, name, connect_addr, port);
	
	// Auf Hello ACK warten
while(helloack != HELLOACK_TYPE) {
	len_recv = Recv(fd,  buffer, (size_t) BUFFER, 0);
	if(ntohs(((struct message *)buffer)->type) == HELLOACK_TYPE) {
printf("start debug helloACK\n");
printf("len_recv:%d\n", len_recv);
printf("hello.type:%d\n", ntohs(((struct message *)buffer)->type));
printf("hello.length:%d\n", ntohs(((struct message *)buffer)->length));
printf("end debug helloACK\n");
	}
	if(checkMsg(fd, (struct message *) buffer, len_recv) == -1) {
		puts("checkMsg error");
		//Close(fd);
		return;
	}
	checkType(fd, (struct message *) buffer);
	helloack = ntohs(((struct message *)buffer)->type);
}
	
	// Auf Peerinfo warten
while(peerinfo != PEERINFO_TYPE) {
	len_recv = Recv(fd,  buffer, (size_t) BUFFER, 0);
	
	if(ntohs(((struct message *)buffer)->type) == PEERINFO_TYPE) {
printf("start debug peerinfo\n");
printf("len_recv:%d\n", len_recv);
printf("peerinfo.type:%d\n", ntohs(((struct message *)buffer)->type));
printf("peerinfo.length:%d\n", ntohs(((struct message *)buffer)->length));
printf("peerinfo.port:%d\n", ntohs(((struct peerinfo *)buffer)->port));
printf("peerinfo.ip:%d\n", ((struct peerinfo *)buffer)->ip_address);
printf("starte ich?:%d\n", ntohs(((struct peerinfo *)buffer)->start));
printf("end debug peerinfo\n");
	}
	if(checkMsg(fd, (struct message *) buffer, len_recv) == -1) {
		puts("checkMsg error");
		//Close(fd);
		return;
	}
	// Sende Peerinfo ACK
	checkType(fd, (struct message *) buffer);
	peerinfo = ntohs(((struct message *)buffer)->type);
}

	// Wenn dann bekommen, Verbindung mit Server abbrechen und mit Peer connecten zum Spielen
	Close(fd);
printf("debug fd close\n");
	connect_peer((struct peerinfo *) buffer);
}

int main(int argc, char *argv[]) {
	connect_server(argv[1], argv[2], argv[3], argv[4]);
	
	init_4clib();
	print_board();
}
