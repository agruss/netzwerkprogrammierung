#include <stdio.h>
#include "messages.h"

#define ALIGN 4

int handleRecv(int fd, struct message *msg, int len_recv) {
	if(checkRecv(fd, msg, len_recv) == -1) {
		return -1;
	}
	checkType(fd, msg);
	
	return 0;
}

int checkRecv(int fd, struct message *msg, int len_recv) {
	if(len_recv < 4) {
		puts("recv error");
		return -1;
	}
	if(len_recv < ntohs(msg->length)) {
		puts("invalid recv length");
		return -1;
	}
	
	return 0;
}

int checkType(int fd, struct message *msg) {
	switch(ntohs(msg->type)) {
		case HELLO_TYPE:
			send_helloACK(fd);
			break;
		case HELLOACK_TYPE:
puts("!!!!!!!Got a helloACK!");
			break;
		case PEERINFO_TYPE:
			send_peerinfoACK(fd);
			break;
		case PEERINFOACK_TYPE:
puts("!!!!!!!Got a peerinfoACK!");
			break;
		case HEARTBEAT_TYPE:
			send_heartbeatACK(fd);
			break;
		case HEARTBEATACK_TYPE:
puts("!!!!!!!Got a heartbeatACK!");
			break;
		default: 
			send_error(fd);
			break;
	}
	
	return 0;
}

void send_hello(int fd, struct in_addr own_addr, const char *name, const char* connect_addr, const char* port) {
	struct hello *msg;
	int length = sizeof(struct hello) + strlen(name) + 1;
	int padding = createPadding(length);
printf("sizeof struct:%d\n", sizeof(struct hello));
printf("strlen+1:%d\n", strlen(name)+1);
printf("size of hello message:%d\n", length);
	msg = (struct hello *) malloc(length + padding);
	
	msg->type = htons(HELLO_TYPE);
	msg->length = htons(length);
	msg->ip_address = own_addr;
	msg->port = htons(atoi(port));
	memcpy((void *) msg->name, (void *) name, strlen(name) + 1);
		
printf("own ip_address:%d\n", msg->ip_address);
printf("own port: %d\n", ntohs(msg->port));
printf("msg->length:%d\n", ntohs(msg->length));
printf("send size:%d\n", (sizeof(msg) + strlen(name) + 1 + padding));
printf("msg.name:%s\n", msg->name);
	Send(fd, (const void *) msg, (length + padding), 0);
	free(msg);
}

void send_helloACK(int fd) {
	struct helloACK *msg;
	int length = sizeof(struct helloACK);
	msg = (struct helloACK *) malloc(length);
	
	msg->type = htons(HELLOACK_TYPE);
	msg->length = htons(length);
	
printf("size of helloACK:%d\n", length);
	send(fd, (const void *) msg, length, 0);
	free(msg);
}

void send_peerinfo(int fd, struct in_addr ip_address, uint16_t port, char *name, int start) {
	struct peerinfo *msg;
	int length = sizeof(struct peerinfo) + strlen(name) + 1;
	int padding = createPadding(length);
	msg = (struct peerinfo *) malloc(length + padding);
	
	msg->type = htons(PEERINFO_TYPE);
	msg->length = htons(length);
	msg->ip_address = ip_address;
	msg->port = port;
	msg->start = htons(start);
	memcpy((void *) msg->name, (void *) name, strlen(name) + 1);
	
printf("others ip_address:%d\n", msg->ip_address);
printf("others port:%d\n", ntohs(msg->port));
printf("starte ich?:%d\n", ntohs(msg->start));
printf("others name:%s\n", msg->name);
printf("send size:%d\n", (length + padding));
	Send(fd, (const void *) msg, (length + padding), 0);
	free(msg);
}

void send_peerinfoACK(int fd) {
	struct peerinfoACK *msg;
	int length = sizeof(struct peerinfoACK);
	msg = (struct peerinfoACK *) malloc(length);
	
	msg->type = htons(PEERINFOACK_TYPE);
	msg->length = htons(length);
	
printf("size of peerinfoACK:%d\n", length);
	Send(fd, (const void *) msg, length, 0);
	free(msg);
}

void send_error(int fd) {
	struct error *msg;
	int length = sizeof(struct error);
	msg = (struct error *) malloc(length);
	
	msg->type = htons(ERROR_TYPE);
	msg->length = htons(length);
	
	Send(fd, (const void *) msg, length, 0);
	free(msg);
}

//TODO Beliebiges Muster einfügen
void send_heartbeat(void *fd) {
	struct heartbeat *msg;
	int length = sizeof(struct heartbeat);
	int padding = createPadding(length);
	msg = (struct heartbeat *) malloc(length + padding);
	
	msg->type = htons(HEARTBEAT_TYPE);
	msg->length = htons(length);
printf("sending heartbeat!\n");
printf("length: %d\n", length);
	send(*((int *) fd), (const void *) msg, (length + padding), 0);
	free(msg);
}

//TODO Das Muster mit einbeziehen
void send_heartbeatACK(int fd) {
	struct heartbeatACK *msg;
	int length = sizeof(struct heartbeatACK);
	int padding = createPadding(length);
	msg = (struct heartbeatACK *) malloc(length + padding);
	
	msg->type = htons(HEARTBEATACK_TYPE);
	msg->length = htons(length);
	
	Send(fd, (const void *) msg, (length + padding), 0);
	free(msg);
}

int createPadding(int length) {
	return ((ALIGN - (length % ALIGN)) % ALIGN);
}
